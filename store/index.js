import Vue from 'vue'
import Vuex from 'vuex'

const state = () => ({})
const mutations = {}
const actions = {}
const getters = {}

export default {
  mutations,
  actions,
  getters,
  state,
}
