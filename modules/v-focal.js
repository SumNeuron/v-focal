// import Vue from 'vue'
// import Vuex from 'vuex'
export const state = () => ({
  focalMode: false,
  focalTime: 1300,
  _focalTimeout: null
})

export const mutations = {
  setFocalMode (state, value) {
    state.focalMode = value
  },
  setFocalTime (state, value) {
    state.focalTime = value
  },
  setFocalTimeout(state, value) {
    state._focalTimeout = value
  },
  clearFocalMode(state) {
    clearTimeout(state._focalTimeout)
    state._focalTimeout = null
  }
}


export const actions = {
  focalMode ({ commit, state }) {
    if (state._focalTimeout) {
      return
    }
    commit('setFocalTimeout', setTimeout(() => commit('setFocalMode', true), state.focalTime))
  },
  clearFocalMode ({ commit, state }) {
    if (state._focalTimeout) {
      commit('clearFocalMode')
    }
    commit('setFocalMode', false)
  },
  focalTime({commit}, value) {
    commit('setFocalTime', value)
  }
}

export const getters = {}

export const namespaced = true
