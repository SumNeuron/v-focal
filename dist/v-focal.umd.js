(function (global, factory) {
  typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports) :
  typeof define === 'function' && define.amd ? define(['exports'], factory) :
  (global = global || self, factory(global.VFocal = {}));
}(this, (function (exports) { 'use strict';

  // import Vue from 'vue'
  // import Vuex from 'vuex'
  const state = () => ({
    focalMode: false,
    focalTime: 1300,
    _focalTimeout: null
  });
  const mutations = {
    setFocalMode(state, value) {
      state.focalMode = value;
    },

    setFocalTime(state, value) {
      state.focalTime = value;
    },

    setFocalTimeout(state, value) {
      state._focalTimeout = value;
    },

    clearFocalMode(state) {
      clearTimeout(state._focalTimeout);
      state._focalTimeout = null;
    }

  };
  const actions = {
    focalMode({
      commit,
      state
    }) {
      if (state._focalTimeout) {
        return;
      }

      commit('setFocalTimeout', setTimeout(() => commit('setFocalMode', true), state.focalTime));
    },

    clearFocalMode({
      commit,
      state
    }) {
      if (state._focalTimeout) {
        commit('clearFocalMode');
      }

      commit('setFocalMode', false);
    },

    focalTime({
      commit
    }, value) {
      commit('setFocalTime', value);
    }

  };
  const getters = {};
  const namespaced = true;

  var VFocalModule = /*#__PURE__*/Object.freeze({
    __proto__: null,
    state: state,
    mutations: mutations,
    actions: actions,
    getters: getters,
    namespaced: namespaced
  });

  //
  var script = {
    name: 'VFocal',
    props: {
      moduleName: {
        type: String,
        default: 'VFocal'
      },
      isFocal: {
        type: Boolean,
        default: true
      },
      isSilent: {
        type: Boolean,
        default: true
      }
    },
    methods: {
      hasModule() {
        let vuexHasModule = this.$store.hasModule(this.moduleName) === true;
        let doubleCheck = this.$store.state && this.$store.state[this.moduleName] !== undefined;
        return vuexHasModule && doubleCheck;
      },

      register() {
        if (!this.hasModule()) {
          try {
            if (!this.hasModule()) {
              this.$store.registerModule(this.moduleName, VFocalModule, {
                preserveState: false
              });
            }
          } catch (e) {
            if (!this.isSilent) {
              console.error(e);
            }
          } finally {
            this.$emit('register:v-focal');
          }
        }
      },

      unregister() {
        if (this.hasModule()) {
          try {
            if (this.hasModule()) {
              this.$store.unregisterModule(this.moduleName);
            }
          } catch (e) {
            if (!this.isSilent) {
              console.error(e);
            }
          } finally {
            this.$emit('unregister:v-focal');
          }
        }
      },

      mouseover() {
        try {
          if (!this.isFocal) {
            this.$store.dispatch(`${this.moduleName}/clearFocalMode`);
            return;
          }

          this.$store.dispatch(`${this.moduleName}/focalMode`);
        } catch (e) {
          console.error(e);
        } finally {}
      },

      mouseleave() {
        try {
          if (!this.isFocal) return;
          this.$store.dispatch(`${this.moduleName}/clearFocalMode`);
        } catch (e) {
          console.error(e);
        } finally {}
      }

    },
    computed: {
      mode() {
        if (this.hasModule()) {
          return this.$store.state[this.moduleName].focalMode;
        }

        return false;
      }

    },

    created() {
      this.register();
    },

    mounted() {
      if (!this.hasModule()) {
        try {
          this.register();
        } catch (e) {} finally {}
      }
    },

    beforeDestroy() {
      try {
        this.unregister();
      } catch (e) {} finally {}
    }

  };

  function normalizeComponent(template, style, script, scopeId, isFunctionalTemplate, moduleIdentifier /* server only */, shadowMode, createInjector, createInjectorSSR, createInjectorShadow) {
      if (typeof shadowMode !== 'boolean') {
          createInjectorSSR = createInjector;
          createInjector = shadowMode;
          shadowMode = false;
      }
      // Vue.extend constructor export interop.
      const options = typeof script === 'function' ? script.options : script;
      // render functions
      if (template && template.render) {
          options.render = template.render;
          options.staticRenderFns = template.staticRenderFns;
          options._compiled = true;
          // functional template
          if (isFunctionalTemplate) {
              options.functional = true;
          }
      }
      // scopedId
      if (scopeId) {
          options._scopeId = scopeId;
      }
      let hook;
      if (moduleIdentifier) {
          // server build
          hook = function (context) {
              // 2.3 injection
              context =
                  context || // cached call
                      (this.$vnode && this.$vnode.ssrContext) || // stateful
                      (this.parent && this.parent.$vnode && this.parent.$vnode.ssrContext); // functional
              // 2.2 with runInNewContext: true
              if (!context && typeof __VUE_SSR_CONTEXT__ !== 'undefined') {
                  context = __VUE_SSR_CONTEXT__;
              }
              // inject component styles
              if (style) {
                  style.call(this, createInjectorSSR(context));
              }
              // register component module identifier for async chunk inference
              if (context && context._registeredComponents) {
                  context._registeredComponents.add(moduleIdentifier);
              }
          };
          // used by ssr in case component is cached and beforeCreate
          // never gets called
          options._ssrRegister = hook;
      }
      else if (style) {
          hook = shadowMode
              ? function (context) {
                  style.call(this, createInjectorShadow(context, this.$root.$options.shadowRoot));
              }
              : function (context) {
                  style.call(this, createInjector(context));
              };
      }
      if (hook) {
          if (options.functional) {
              // register for functional component in vue file
              const originalRender = options.render;
              options.render = function renderWithStyleInjection(h, context) {
                  hook.call(context);
                  return originalRender(h, context);
              };
          }
          else {
              // inject component registration as beforeCreate hook
              const existing = options.beforeCreate;
              options.beforeCreate = existing ? [].concat(existing, hook) : [hook];
          }
      }
      return script;
  }

  const isOldIE = typeof navigator !== 'undefined' &&
      /msie [6-9]\\b/.test(navigator.userAgent.toLowerCase());
  function createInjector(context) {
      return (id, style) => addStyle(id, style);
  }
  let HEAD;
  const styles = {};
  function addStyle(id, css) {
      const group = isOldIE ? css.media || 'default' : id;
      const style = styles[group] || (styles[group] = { ids: new Set(), styles: [] });
      if (!style.ids.has(id)) {
          style.ids.add(id);
          let code = css.source;
          if (css.map) {
              // https://developer.chrome.com/devtools/docs/javascript-debugging
              // this makes source maps inside style tags work properly in Chrome
              code += '\n/*# sourceURL=' + css.map.sources[0] + ' */';
              // http://stackoverflow.com/a/26603875
              code +=
                  '\n/*# sourceMappingURL=data:application/json;base64,' +
                      btoa(unescape(encodeURIComponent(JSON.stringify(css.map)))) +
                      ' */';
          }
          if (!style.element) {
              style.element = document.createElement('style');
              style.element.type = 'text/css';
              if (css.media)
                  style.element.setAttribute('media', css.media);
              if (HEAD === undefined) {
                  HEAD = document.head || document.getElementsByTagName('head')[0];
              }
              HEAD.appendChild(style.element);
          }
          if ('styleSheet' in style.element) {
              style.styles.push(code);
              style.element.styleSheet.cssText = style.styles
                  .filter(Boolean)
                  .join('\n');
          }
          else {
              const index = style.ids.size - 1;
              const textNode = document.createTextNode(code);
              const nodes = style.element.childNodes;
              if (nodes[index])
                  style.element.removeChild(nodes[index]);
              if (nodes.length)
                  style.element.insertBefore(textNode, nodes[index]);
              else
                  style.element.appendChild(textNode);
          }
      }
  }

  /* script */
  const __vue_script__ = script;
  /* template */

  var __vue_render__ = function () {
    var _vm = this;

    var _h = _vm.$createElement;

    var _c = _vm._self._c || _h;

    return _c('div', {
      staticClass: "v-focal opacity-transition",
      class: {
        'v-focal--focaled': _vm.isFocal && _vm.mode,
        'v-focal--unfocaled': !_vm.isFocal && _vm.mode
      },
      on: {
        "mouseover": _vm.mouseover,
        "mouseleave": _vm.mouseleave
      }
    }, [_vm._t("default")], 2);
  };

  var __vue_staticRenderFns__ = [];
  /* style */

  const __vue_inject_styles__ = function (inject) {
    if (!inject) return;
    inject("data-v-2de9f03e_0", {
      source: ".v-focal{opacity:1}.v-focal--focaled{opacity:1}.v-focal--unfocaled{opacity:.25}.opacity-transition{transition:opacity .5s ease-out}.invisible{display:none;opacity:0;max-width:0;max-height:0}",
      map: undefined,
      media: undefined
    });
  };
  /* scoped */


  const __vue_scope_id__ = undefined;
  /* module identifier */

  const __vue_module_identifier__ = undefined;
  /* functional template */

  const __vue_is_functional_template__ = false;
  /* style inject SSR */

  /* style inject shadow dom */

  const __vue_component__ = /*#__PURE__*/normalizeComponent({
    render: __vue_render__,
    staticRenderFns: __vue_staticRenderFns__
  }, __vue_inject_styles__, __vue_script__, __vue_scope_id__, __vue_is_functional_template__, __vue_module_identifier__, false, createInjector, undefined, undefined);

  // Import vue components
  const components = {
    VFocal: __vue_component__
  }; // install function executed by Vue.use()

  const install = function installVFocus(Vue) {
    if (install.installed) return;
    install.installed = true;
    Object.entries(components).forEach(([componentName, component]) => {
      Vue.component(componentName, component);
    });
  }; // Create module definition for Vue.use()


  const plugin = {
    install
  }; // To auto-install on non-es builds, when vue is found
  // eslint-disable-next-line no-redeclare

  /* global window, global */

  {
    let GlobalVue = null;

    if (typeof window !== 'undefined') {
      GlobalVue = window.Vue;
    } else if (typeof global !== 'undefined') {
      GlobalVue = global.Vue;
    }

    if (GlobalVue) {
      GlobalVue.use(plugin);
    }
  } // Default export is library as a whole, registered via Vue.use()

  exports.VFocal = __vue_component__;
  exports.VFocalModule = VFocalModule;
  exports.default = plugin;

  Object.defineProperty(exports, '__esModule', { value: true });

})));
