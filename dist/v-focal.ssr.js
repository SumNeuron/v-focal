'use strict';Object.defineProperty(exports,'__esModule',{value:true});function _arrayWithHoles(arr) {
  if (Array.isArray(arr)) return arr;
}

var arrayWithHoles = _arrayWithHoles;function _iterableToArrayLimit(arr, i) {
  if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return;
  var _arr = [];
  var _n = true;
  var _d = false;
  var _e = undefined;

  try {
    for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) {
      _arr.push(_s.value);

      if (i && _arr.length === i) break;
    }
  } catch (err) {
    _d = true;
    _e = err;
  } finally {
    try {
      if (!_n && _i["return"] != null) _i["return"]();
    } finally {
      if (_d) throw _e;
    }
  }

  return _arr;
}

var iterableToArrayLimit = _iterableToArrayLimit;function _arrayLikeToArray(arr, len) {
  if (len == null || len > arr.length) len = arr.length;

  for (var i = 0, arr2 = new Array(len); i < len; i++) {
    arr2[i] = arr[i];
  }

  return arr2;
}

var arrayLikeToArray = _arrayLikeToArray;function _unsupportedIterableToArray(o, minLen) {
  if (!o) return;
  if (typeof o === "string") return arrayLikeToArray(o, minLen);
  var n = Object.prototype.toString.call(o).slice(8, -1);
  if (n === "Object" && o.constructor) n = o.constructor.name;
  if (n === "Map" || n === "Set") return Array.from(o);
  if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return arrayLikeToArray(o, minLen);
}

var unsupportedIterableToArray = _unsupportedIterableToArray;function _nonIterableRest() {
  throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.");
}

var nonIterableRest = _nonIterableRest;function _slicedToArray(arr, i) {
  return arrayWithHoles(arr) || iterableToArrayLimit(arr, i) || unsupportedIterableToArray(arr, i) || nonIterableRest();
}

var slicedToArray = _slicedToArray;// import Vue from 'vue'
// import Vuex from 'vuex'
var state = function state() {
  return {
    focalMode: false,
    focalTime: 1300,
    _focalTimeout: null
  };
};
var mutations = {
  setFocalMode: function setFocalMode(state, value) {
    state.focalMode = value;
  },
  setFocalTime: function setFocalTime(state, value) {
    state.focalTime = value;
  },
  setFocalTimeout: function setFocalTimeout(state, value) {
    state._focalTimeout = value;
  },
  clearFocalMode: function clearFocalMode(state) {
    clearTimeout(state._focalTimeout);
    state._focalTimeout = null;
  }
};
var actions = {
  focalMode: function focalMode(_ref) {
    var commit = _ref.commit,
        state = _ref.state;

    if (state._focalTimeout) {
      return;
    }

    commit('setFocalTimeout', setTimeout(function () {
      return commit('setFocalMode', true);
    }, state.focalTime));
  },
  clearFocalMode: function clearFocalMode(_ref2) {
    var commit = _ref2.commit,
        state = _ref2.state;

    if (state._focalTimeout) {
      commit('clearFocalMode');
    }

    commit('setFocalMode', false);
  },
  focalTime: function focalTime(_ref3, value) {
    var commit = _ref3.commit;
    commit('setFocalTime', value);
  }
};
var getters = {};
var namespaced = true;var VFocalModule=/*#__PURE__*/Object.freeze({__proto__:null,state: state,mutations: mutations,actions: actions,getters: getters,namespaced: namespaced});//
var script = {
  name: 'VFocal',
  props: {
    moduleName: {
      type: String,
      default: 'VFocal'
    },
    isFocal: {
      type: Boolean,
      default: true
    },
    isSilent: {
      type: Boolean,
      default: true
    }
  },
  methods: {
    hasModule: function hasModule() {
      var vuexHasModule = this.$store.hasModule(this.moduleName) === true;
      var doubleCheck = this.$store.state && this.$store.state[this.moduleName] !== undefined;
      return vuexHasModule && doubleCheck;
    },
    register: function register() {
      if (!this.hasModule()) {
        try {
          if (!this.hasModule()) {
            this.$store.registerModule(this.moduleName, VFocalModule, {
              preserveState: false
            });
          }
        } catch (e) {
          if (!this.isSilent) {
            console.error(e);
          }
        } finally {
          this.$emit('register:v-focal');
        }
      }
    },
    unregister: function unregister() {
      if (this.hasModule()) {
        try {
          if (this.hasModule()) {
            this.$store.unregisterModule(this.moduleName);
          }
        } catch (e) {
          if (!this.isSilent) {
            console.error(e);
          }
        } finally {
          this.$emit('unregister:v-focal');
        }
      }
    },
    mouseover: function mouseover() {
      try {
        if (!this.isFocal) {
          this.$store.dispatch("".concat(this.moduleName, "/clearFocalMode"));
          return;
        }

        this.$store.dispatch("".concat(this.moduleName, "/focalMode"));
      } catch (e) {
        console.error(e);
      } finally {}
    },
    mouseleave: function mouseleave() {
      try {
        if (!this.isFocal) return;
        this.$store.dispatch("".concat(this.moduleName, "/clearFocalMode"));
      } catch (e) {
        console.error(e);
      } finally {}
    }
  },
  computed: {
    mode: function mode() {
      if (this.hasModule()) {
        return this.$store.state[this.moduleName].focalMode;
      }

      return false;
    }
  },
  created: function created() {
    this.register();
  },
  mounted: function mounted() {
    if (!this.hasModule()) {
      try {
        this.register();
      } catch (e) {} finally {}
    }
  },
  beforeDestroy: function beforeDestroy() {
    try {
      this.unregister();
    } catch (e) {} finally {}
  }
};function normalizeComponent(template, style, script, scopeId, isFunctionalTemplate, moduleIdentifier /* server only */, shadowMode, createInjector, createInjectorSSR, createInjectorShadow) {
    if (typeof shadowMode !== 'boolean') {
        createInjectorSSR = createInjector;
        createInjector = shadowMode;
        shadowMode = false;
    }
    // Vue.extend constructor export interop.
    const options = typeof script === 'function' ? script.options : script;
    // render functions
    if (template && template.render) {
        options.render = template.render;
        options.staticRenderFns = template.staticRenderFns;
        options._compiled = true;
        // functional template
        if (isFunctionalTemplate) {
            options.functional = true;
        }
    }
    // scopedId
    if (scopeId) {
        options._scopeId = scopeId;
    }
    let hook;
    if (moduleIdentifier) {
        // server build
        hook = function (context) {
            // 2.3 injection
            context =
                context || // cached call
                    (this.$vnode && this.$vnode.ssrContext) || // stateful
                    (this.parent && this.parent.$vnode && this.parent.$vnode.ssrContext); // functional
            // 2.2 with runInNewContext: true
            if (!context && typeof __VUE_SSR_CONTEXT__ !== 'undefined') {
                context = __VUE_SSR_CONTEXT__;
            }
            // inject component styles
            if (style) {
                style.call(this, createInjectorSSR(context));
            }
            // register component module identifier for async chunk inference
            if (context && context._registeredComponents) {
                context._registeredComponents.add(moduleIdentifier);
            }
        };
        // used by ssr in case component is cached and beforeCreate
        // never gets called
        options._ssrRegister = hook;
    }
    else if (style) {
        hook = shadowMode
            ? function (context) {
                style.call(this, createInjectorShadow(context, this.$root.$options.shadowRoot));
            }
            : function (context) {
                style.call(this, createInjector(context));
            };
    }
    if (hook) {
        if (options.functional) {
            // register for functional component in vue file
            const originalRender = options.render;
            options.render = function renderWithStyleInjection(h, context) {
                hook.call(context);
                return originalRender(h, context);
            };
        }
        else {
            // inject component registration as beforeCreate hook
            const existing = options.beforeCreate;
            options.beforeCreate = existing ? [].concat(existing, hook) : [hook];
        }
    }
    return script;
}function createInjectorSSR(context) {
    if (!context && typeof __VUE_SSR_CONTEXT__ !== 'undefined') {
        context = __VUE_SSR_CONTEXT__;
    }
    if (!context)
        return () => { };
    if (!('styles' in context)) {
        context._styles = context._styles || {};
        Object.defineProperty(context, 'styles', {
            enumerable: true,
            get: () => context._renderStyles(context._styles)
        });
        context._renderStyles = context._renderStyles || renderStyles;
    }
    return (id, style) => addStyle(id, style, context);
}
function addStyle(id, css, context) {
    const group =  css.media || 'default' ;
    const style = context._styles[group] || (context._styles[group] = { ids: [], css: '' });
    if (!style.ids.includes(id)) {
        style.media = css.media;
        style.ids.push(id);
        let code = css.source;
        style.css += code + '\n';
    }
}
function renderStyles(styles) {
    let css = '';
    for (const key in styles) {
        const style = styles[key];
        css +=
            '<style data-vue-ssr-id="' +
                Array.from(style.ids).join(' ') +
                '"' +
                (style.media ? ' media="' + style.media + '"' : '') +
                '>' +
                style.css +
                '</style>';
    }
    return css;
}/* script */
var __vue_script__ = script;
/* template */

var __vue_render__ = function __vue_render__() {
  var _vm = this;

  var _h = _vm.$createElement;

  var _c = _vm._self._c || _h;

  return _c('div', {
    staticClass: "v-focal opacity-transition",
    class: {
      'v-focal--focaled': _vm.isFocal && _vm.mode,
      'v-focal--unfocaled': !_vm.isFocal && _vm.mode
    },
    on: {
      "mouseover": _vm.mouseover,
      "mouseleave": _vm.mouseleave
    }
  }, [_vm._t("default")], 2);
};

var __vue_staticRenderFns__ = [];
/* style */

var __vue_inject_styles__ = function __vue_inject_styles__(inject) {
  if (!inject) return;
  inject("data-v-2de9f03e_0", {
    source: ".v-focal{opacity:1}.v-focal--focaled{opacity:1}.v-focal--unfocaled{opacity:.25}.opacity-transition{transition:opacity .5s ease-out}.invisible{display:none;opacity:0;max-width:0;max-height:0}",
    map: undefined,
    media: undefined
  });
};
/* scoped */


var __vue_scope_id__ = undefined;
/* module identifier */

var __vue_module_identifier__ = "data-v-2de9f03e";
/* functional template */

var __vue_is_functional_template__ = false;
/* style inject shadow dom */

var __vue_component__ = /*#__PURE__*/normalizeComponent({
  render: __vue_render__,
  staticRenderFns: __vue_staticRenderFns__
}, __vue_inject_styles__, __vue_script__, __vue_scope_id__, __vue_is_functional_template__, __vue_module_identifier__, false, undefined, createInjectorSSR, undefined);var components = {
  VFocal: __vue_component__
}; // install function executed by Vue.use()

var install = function installVFocus(Vue) {
  if (install.installed) return;
  install.installed = true;
  Object.entries(components).forEach(function (_ref) {
    var _ref2 = slicedToArray(_ref, 2),
        componentName = _ref2[0],
        component = _ref2[1];

    Vue.component(componentName, component);
  });
}; // Create module definition for Vue.use()


var plugin = {
  install: install
}; // To auto-install on non-es builds, when vue is found
// eslint-disable-next-line no-redeclare

/* global window, global */

{
  var GlobalVue = null;

  if (typeof window !== 'undefined') {
    GlobalVue = window.Vue;
  } else if (typeof global !== 'undefined') {
    GlobalVue = global.Vue;
  }

  if (GlobalVue) {
    GlobalVue.use(plugin);
  }
} // Default export is library as a whole, registered via Vue.use()
exports.VFocal=__vue_component__;exports.VFocalModule=VFocalModule;exports.default=plugin;